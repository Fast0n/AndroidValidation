package com.fast0n.validation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText uno, due;
    Button press;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        uno = (EditText)findViewById(R.id.editText);
        due = (EditText)findViewById(R.id.editText2);
        press = (Button)findViewById(R.id.button);

        press.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uno.getText().toString().equals(""))
                    uno.setError(getString(R.string.error));

                else if (due.getText().toString().equals(""))
                    due.setError(getString(R.string.error));

                else
                    Toast.makeText(MainActivity.this, "OK", Toast.LENGTH_LONG).show();

            }
        });

    }
}
